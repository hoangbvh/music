package huyencool.com.music2.view;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import huyencool.com.music2.Library;
import huyencool.com.music2.R;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.model.Constant;
import huyencool.com.music2.view.new_interface.MusicPlayListener;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class MediaPlaybackFragment extends Fragment {
    private BaiHat baiHatActive;
    ArrayList<BaiHat> listBaiHat;
    TextView nameMusic;
    TextView artist;
    ImageView smallImageMusic;
    ImageView bigImageMusic;
    ImageView nextMusicButton;
    ImageView backToListButton;
    SeekBar seekBar;
    TextView timeBatdau;
    TextView timeKetthuc;
    ImageView pausePlayButtom;
    ImageView playButton;       //button play in header
    ImageView prevButton;
    ImageView likeButton;
    ImageView disLikeButton;
    ImageView moreButton;
    int sTime = 1000;
    int eTime = 20000;
    boolean isPlaying = true;
    int positionActive = 0;

    //
    Boolean isFavorite = false;
    Boolean isDisLike = false;
    LinearLayout viewPlayButton;
    LinearLayout viewBackAndMoreButton;
    private ArrayList<BaiHat> listFavorite = new ArrayList<>();
    private ArrayList<BaiHat> listDisLike = new ArrayList<>();
    //
    ConstraintLayout headerMediaPlayer;
    ViewPlayerListener viewPlayerListener;

    //
    MusicPlayListener musicPlayListener;
    public void setListBaiHat(ArrayList<BaiHat> list){
        this.listBaiHat = list;
    }
    public void setPositionActive(int positionActive){
        this.positionActive = positionActive;
    }
    public void setMusicPlayListener(MusicPlayListener musicPlayListener){
        this.musicPlayListener = musicPlayListener;
    }

    public void setViewPlayerListener(ViewPlayerListener viewPlayerListener){
        this.viewPlayerListener = viewPlayerListener;
    }
    public MediaPlaybackFragment(){

    }
    public void setBaiHatActive(BaiHat baiHatActive){
        this.baiHatActive = baiHatActive;
    }

    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            sTime = Integer.parseInt(intent.getStringExtra("currentTime"));
            eTime = Integer.parseInt(intent.getStringExtra("maxTime"));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seekBar.setProgress(sTime/1000);
                    seekBar.setMax(eTime/1000);
                    long phutKetthuc = TimeUnit.MILLISECONDS.toMinutes((long) eTime);
                    long giayKetthuc = TimeUnit.MILLISECONDS.toSeconds((long) eTime) - (phutKetthuc * 60);
                    timeKetthuc.setText(String.format("%d : %d", phutKetthuc, giayKetthuc));
                }
            });
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(bReceiver, new IntentFilter("message")); // dki vs service
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(bReceiver); // huy dki service
    }
    private void changeSeekBar(int s, int e){
        if (!isPlaying)
            return;
        seekBar.setProgress(sTime / 1000); // hien thi time chay
        Log.d("asda",eTime + "");
        if (sTime/1000 >= eTime/1000) {

            musicPlayListener.onNext(positionActive);
            int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
            positionActive = p;
            sTime = 1000;
            updateUI(listBaiHat.get(p));
            changeSeekBar(sTime, eTime);
        }
        else{
            sTime = s + 1000;
            // gán lại thời gian của bài hát đang hát bi dung
            eTime = e;
            final int tmp = s + 1000;
            final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
            final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
            new Handler().postDelayed(new Runnable() { // hàm chơ 1 giây sau thực hiện tiếp
                @Override
                public void run() {
                    if (tmp == sTime){
                        timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));
                    }
                    changeSeekBar(sTime,eTime);
                }
            },1000);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media_playback2, container, false);
         nameMusic = view.findViewById(R.id.musicName);
         artist = view.findViewById(R.id.musicArtist);
         smallImageMusic = view.findViewById(R.id.smallImageMusic);
         bigImageMusic = view.findViewById(R.id.musicBigImageView);
         seekBar = view.findViewById(R.id.seekBar);
         timeBatdau = view.findViewById(R.id.startTimeTextView);
         timeKetthuc = view.findViewById(R.id.endTimeTextView);
         pausePlayButtom = view.findViewById(R.id.pausePlayButton);
         nextMusicButton = view.findViewById(R.id.nextMusicButton);
        prevButton = view.findViewById(R.id.prevButton);
        likeButton = view.findViewById(R.id.likeButton);
        disLikeButton = view.findViewById(R.id.disLikeButton);
        moreButton = view.findViewById(R.id.moreButton);
         // header view
        viewBackAndMoreButton = view.findViewById(R.id.viewBackAndMoreButton);
        viewPlayButton = view.findViewById(R.id.viewPlayButton);

        loadFromCache();
        changeSeekBar(sTime,eTime);
        for (BaiHat bh : listFavorite){
            if (baiHatActive == null){
                break;
            }
            if (bh.getUri().equals(baiHatActive.getUri())){
                likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                isFavorite = true;
                break;
            }
        }
        for (BaiHat bh : listDisLike){
            if (baiHatActive == null){
                break;
            }
            if (bh.getUri().equals(baiHatActive.getUri())){
                disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                isDisLike = true;
                break;
            }
        }
        likeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isFavorite){
                    listFavorite.remove(baiHatActive);
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                else{
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                    listFavorite.add(baiHatActive);
                }
                if (isDisLike){
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                    listDisLike.remove(baiHatActive);
                }

                BaiHat.saveToShared(getContext(),listDisLike,Constant.DIS_LIKE);
                BaiHat.saveToShared(getContext(),listFavorite,Constant.FAVORITE_MUSIC);
            }
        });
        disLikeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isDisLike){
                    listDisLike.remove(baiHatActive);
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                else{
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                    listDisLike.add(baiHatActive);
                }
                if (isFavorite){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                    listFavorite.remove(baiHatActive);
                }
                BaiHat.saveToShared(getContext(),listDisLike,Constant.DIS_LIKE);
                BaiHat.saveToShared(getContext(),listFavorite,Constant.FAVORITE_MUSIC);
            }
        });
        moreButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(),view);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        viewPlayerListener.onCollapseView();
                        switch (menuItem.getItemId()) {
                            case R.id.listFavorite:
                                ArrayList<BaiHat> listFavorite = getFavoriteMusic();
                                viewPlayerListener.updateListMusic(listFavorite);
                                return true;
                            case R.id.listDislike:
                                ArrayList<BaiHat> listDisLike = getDislikeMusic();
                                viewPlayerListener.updateListMusic(listDisLike);
                            default:
                                return false;
                        }
                    }
                });
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.menu_media, popupMenu.getMenu());
                popupMenu.show();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
              //  musicPlayListener.seekTo(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @SuppressLint("DefaultLocale")
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                musicPlayListener.seekTo(baiHatActive,seekBar.getProgress() * 1000);
                sTime = seekBar.getProgress() * 1000;
                final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
                timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));

            }
        });
         //header media player
        headerMediaPlayer = view.findViewById(R.id.headerMediaPlayer);
        headerMediaPlayer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
                viewPlayButton.setVisibility(View.GONE);
                viewPlayerListener.onTouchToOpen();
            }
        });
        //
        backToListButton = view.findViewById(R.id.backToListButton);
        backToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
                viewPlayButton.setVisibility(View.VISIBLE);
                viewPlayerListener.onCollapseView();
            }
        });
        playButton = view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateState();

            }
        });

         pausePlayButtom . setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 updateState();
             }
         });
         nextMusicButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 musicPlayListener.onNext(positionActive);
                 int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                 positionActive = p;
                 updateUI(listBaiHat.get(p));
             }
         });
        prevButton.setOnClickListener(new View.OnClickListener( ){
            @Override
            public void onClick(View view) {
                musicPlayListener.onPrev(positionActive);
                int p = (positionActive == 0) ? listBaiHat.size() - 1 : positionActive - 1;
                positionActive = p;
                updateUI(listBaiHat.get(p));
            }
        });
        if (baiHatActive != null)
        updateUI(baiHatActive);
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);

    }

    private void updateState(){
        if(isPlaying){
            musicPlayListener.onPause();
            playButton.setImageResource(R.drawable.ic_play_black_round);
            pausePlayButtom.setImageResource(R.drawable.ic_play_black_round);
            isPlaying = false;
        }
        else{
            isPlaying = true;
            musicPlayListener.onPlay();
            changeSeekBar(sTime,eTime);
            playButton.setImageResource(R.drawable.ic_pause_black_large);
            pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
        }


    }

    public void updateUI(BaiHat baiHat){
        timeBatdau.setText(String.format("%d : %d", 0, 0));
        sTime = 0;
        baiHatActive = baiHat;
        musicPlayListener.onGetTime(baiHat);
        smallImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        nameMusic.setText(baiHat.getmTenbaihat());
        artist.setText(baiHat.getmTencasi());
        bigImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
    }

    private void loadFromCache(){
        ArrayList<BaiHat> bF = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        ArrayList<BaiHat> bD = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        if (bF != null){
            listFavorite = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        }
        if (bD != null){
            listDisLike = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        }
    }


    private ArrayList<BaiHat> getFavoriteMusic(){
        ArrayList<BaiHat> result;
        result = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        return result;
    }

    private ArrayList<BaiHat> getDislikeMusic(){
        ArrayList<BaiHat> result;
        result = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        return result;
    }
}
