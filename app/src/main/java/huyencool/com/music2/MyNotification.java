package huyencool.com.music2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;

import huyencool.com.music2.model.BaiHat;

public class MyNotification extends Notification  implements Parcelable {
    private Context parent;
    private NotificationManager nManager;
    private NotificationCompat.Builder nBuilder;
    private RemoteViews remoteView;
    private  BaiHat baiHat;
    private ArrayList<BaiHat> mBaihat;
    @SuppressLint("NewApi")

    private static final String  CHANNEL_ID = "SYMPER_1";
    public void createNotification(BaiHat baiHat){
        this.baiHat = baiHat;

        remoteView = new RemoteViews(parent.getPackageName(), R.layout.notifications);
        remoteView.setTextViewText(R.id.baihat,this.baiHat.getmTenbaihat());
        remoteView.setTextViewText(R.id.casi,this.baiHat.getmTencasi());
        remoteView.setImageViewBitmap(R.id.imageView2,Library.StringToBitMap(baiHat.getAnh()));

        nBuilder = new NotificationCompat.Builder(parent, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_pause_black_large)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

        setListeners(remoteView);
        nBuilder.setContent(remoteView);
        nManager = (NotificationManager) parent.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, nBuilder.build());
    }
    public MyNotification(Context ctx) {
        super();
        this.parent = ctx;

    }
    private void setListeners(RemoteViews view){
        Intent prev = new Intent("prev");
        prev.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent prevButton = PendingIntent.getBroadcast(parent,0,prev,0);
        view.setOnClickPendingIntent(R.id.prevButton, prevButton);

        //listener 2
        Intent stop = new Intent(parent, HelperActivity.class);
        stop.setAction("stop");
        PendingIntent btn2 = PendingIntent.getActivity(parent, 1, stop, 0);
        view.setOnClickPendingIntent(R.id.pausePlayButton, btn2);
        //listener 2
        Intent next = new Intent(parent, HelperActivity.class);
        next.setAction("next");
        PendingIntent nextButton = PendingIntent.getActivity(parent, 1, next, 0);
        view.setOnClickPendingIntent(R.id.nextButton, nextButton);
    }
    public static class DownloadCancelReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("assadasd","ok");
            System.out.println("Received Cancelled Event");
        }
    }
}






























